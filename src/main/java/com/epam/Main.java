package com.epam;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Package for my developing.
 *
 * @author Nazarii Yaremchuk
 * @since 09-11-2019
 */

public final class Main {
    /**
     * private-access constructor.
     */
    private Main() {
        throw new AssertionError("Instantiating utility class...");
    }

    /**
     * The lab program implements implements the application,
     * that form Fibonacci sequance.
     * Start numbers of Fibonacci sequance are last numbers
     * of entered by user range.
     * User enter the interval(for example: [1;100])
     * Program prints odd numbers form start to the end
     * of interval and even from end to start
     * Program prints the sim of odd and even numbers
     * Program prints percentage of odd and even Fibonacci numbers
     *
     * @param args You can enter some String parameters from command line
     */
    public static void main(final String[] args) {
        int fibonacciSize = 0;
        Limit limit = new Limit();
        Scanner scanner = new Scanner(System.in);

        limit.initLimit();
        limit.printOddNumbersFromStart();
        limit.printEvenNumbersFromEnd();

        System.out.println("Enter size of Fibonacci numbers "
                + "you want to build : ");
        fibonacciSize = scanner.nextInt();
        FibonacciSequance fibonacciSequance = new FibonacciSequance(
                fibonacciSize, limit.getMaxOdd(), limit.getMaxEven());
        System.out.println("Builded Fibonacci numbers: ");
        System.out.println(Arrays.toString(
                fibonacciSequance.getFibonacciArray()));
        System.out.println(fibonacciSequance.getReport());
    }
}
