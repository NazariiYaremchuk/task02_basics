package com.epam;

/**
 * Class describes Fibonacci sequance.
 *
 * @author Nazarii Yaremchuk
 * @since 09-11-2019
 */
public class FibonacciSequance {
    /**
     * main fibonacci array.
     */
    private int[] fibonacciArray;
    /**
     * number of odd elements.
     */
    private int oddFibonacciNumbers;
    /**
     * number of even elements.
     */
    private int evenFibonacciNumbers;
    /**
     * size of sequance.
     */
    private final int fibonacciSize;
    /**
     * equals 100.
     */
    private static final int MAX_PRESENT = 100;

    /**
     * @param externalFibonacciSize Size of sequance
     * @param number1               First number of sequance
     * @param number2               Second number of sequance
     */
    FibonacciSequance(final int externalFibonacciSize,
                      final int number1, final int number2) {
        this.fibonacciSize = externalFibonacciSize;
        fibonacciArray = new int[fibonacciSize];
        fibonacciArray[0] = number1;
        fibonacciArray[1] = number2;
        for (int i = 2; i < fibonacciSize; i++) {
            fibonacciArray[i] = fibonacciArray[i - 2] + fibonacciArray[i - 1];
        }
        oddFibonacciNumbers = 0;
        evenFibonacciNumbers = 0;
        calculatePercentages();
    }

    /**
     * Method calculates how many even and odd
     * numbers in sequance and set relevant variables.
     */
    private void calculatePercentages() {
        for (int i = 0; i < fibonacciSize; i++) {
            if (fibonacciArray[i] % 2 == 0) {
                evenFibonacciNumbers++;
            } else {
                oddFibonacciNumbers++;
            }
        }
    }

    /**
     * @return int[] array with fibonacci numbers.
     */
    public final int[] getFibonacciArray() {
        return fibonacciArray;
    }

    /**
     * @return String about odd an even fibonacci numbers.
     */
    public final String getReport() {
        return "\nOdd Fibonacci numbers in builded series: "
                + (double) oddFibonacciNumbers / fibonacciSize * MAX_PRESENT
                + '%' + ", even: " + (double) evenFibonacciNumbers
                / fibonacciSize
                * MAX_PRESENT + '%';
    }
}
