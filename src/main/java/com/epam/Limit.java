package com.epam;

import java.util.Scanner;

/**
 * Class Limit describes the range, the limits of which were entered by user.
 *
 * @author Nazarii Yaremchuk
 * @since 09-11-2019
 */

public class Limit {
    /**
     * Start limit of range.
     */
    private int startLimit;
    /**
     * End limit of range.
     */
    private int endLimit;
    /**
     * max odd element of sequance.
     */
    private int maxOdd = 0;
    /**
     * max even element of sequance.
     */
    private int maxEven = 0;

    /**
     * Constructor of Range, which contains two limits: startRange and endRange.
     */
    Limit() {
        startLimit = 0;
        endLimit = 0;
    }

    /**
     * @param extStartLimit The start limit of range
     * @param extEndLimit   The end limit of range
     */
    Limit(final int extStartLimit, final int extEndLimit) {
        this.startLimit = extStartLimit;
        this.endLimit = extEndLimit;
        checkMaxValues();
    }

    /**
     * User enters range limits, then method checks max odd
     * and even values in range with method checkMaxValues.
     */
    public final void initLimit() {
        System.out.println("Enter limits:");
        Scanner scanner = new Scanner(System.in);
        startLimit = scanner.nextInt();
        endLimit = scanner.nextInt();
        System.out.println("Enterred Limit :" + '[' + startLimit
                + "," + endLimit + "]\n");
        checkMaxValues();
    }

    /**
     * Method update values in maxEven and MaxOdd.
     */
    public final void checkMaxValues() {
        if (endLimit % 2 == 0) {
            maxEven = endLimit;
            maxOdd = endLimit - 1;
        } else {
            maxOdd = endLimit;
            maxEven = endLimit - 1;
        }
    }

    /**
     * method prints odd numbers from start of fibonacci sequance.
     */
    public final void printOddNumbersFromStart() {
        System.out.println("Odd numbers from start:");
        int sumNumbers = 0;
        int startPont;
        if (startLimit % 2 == 0) {
            startPont = startLimit + 1;
        } else {
            startPont = startLimit;
        }
        for (int i = startPont;
             i <= endLimit; i += 2) {
            sumNumbers += i;
            System.out.println(i + " ");
        }
        System.out.println("\nSum of odd numbers: " + sumNumbers);
    }

    /**
     * method prints even numbers from end of fibonacci sequance.
     */
    public final void printEvenNumbersFromEnd() {
        System.out.println("Even numbers from end: ");
        int sumNumbers = 0;
        int startPoint;
        if (startLimit % 2 == 0) {
            startPoint = endLimit;
        } else {
            startPoint = endLimit - 1;
        }
        for (int i = startPoint; i >= startPoint; i -= 2) {
            sumNumbers += i;
            System.out.println(i + " ");

        }
        System.out.println("\nSum of even numbers: " + sumNumbers);
    }

    /**
     * @return max odd element from fibonacci sequance
     */
    public final int getMaxOdd() {
        return maxOdd;
    }

    /**
     * @return max even element from fibonacci sequance
     */
    public final int getMaxEven() {
        return maxEven;
    }
}
